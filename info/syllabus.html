<html>
<head>
<style>
  body { max-width: 8in; margin: 1in; }
  table { border: 1px solid black; border-collapse: collapse; }
  th, td { border: 1px solid black; padding: 0.3em; }
  th { text-align: left; }
  td { text-align: center; }
</style>
</head>
<body>

<h1>CS 410/510 Code Reading &amp; Review, Winter 2024</h1>
<h2>Syllabus</h2>

<h3>Academic misconduct</h3>
  <p>All assignments are <strong>individual work</strong> unless the assignment explicitly says that group work is allowed.</p>
  <p>When you submit individual work, you are implicitly claiming that <strong>you alone</strong> are the author of the content that you are submitting, except any content that you have explicitly cited a source for. If this claim is not true, your submission is plagiarism.</p>
  <p>If you <strong>implicitly or explicitly</strong> claim authorship of anything that you did not personally write, you will get a grade of zero on your submission. This also applies if you knowingly allow someone else to claim authorship of your own work. If it happens more than once, I will write you up for academic misconduct.</p>
  <p>Review PSU's rules at <a href="https://www.pdx.edu/dos/academic-misconduct">https://www.pdx.edu/dos/academic-misconduct</a>.</p>

<h3>Repository</h3>
  <div>The syllabus and schedule for this course will be tracked in a GitLab repository at <a href="https://gitlab.cecs.pdx.edu/cas28/code-reading-and-review-winter-2024">https://gitlab.cecs.pdx.edu/cas28/code-reading-and-review-winter-2024</a>. This will let you see the history of any changes that I may make to these documents throughout the quarter.</div>

<h3>Course staff</h3>
  <p>Please <a href="mailto:cas28@pdx.edu,coltharp@pdx.edu">email <b>both of us</b></a> if you have a question!</p>
  <table>
    <tr><td>
      <div>Katie Casamento</div>
      <div>Email: cas28@pdx.edu</div>
      <div>Office hours: Tuesday 5:30-6:30pm, in the fishbowl or my office (FAB 115D), or at <a href="https://pdx.zoom.us/j/85842958142">https://pdx.zoom.us/j/85842958142</a></div>

      <p>FAB 115D is in the CS offices behind the fishbowl, near the bottom-right of page 9 in the <a href="https://www.pdx.edu/buildings/sites/g/files/znldhr2301/files/2021-10/Fourth%20Avenue%20Building%20Floorplans.pdf">building plan</a> (where L120-00 is the fishbowl).</p>
    </td></tr>
    <tr><td>
      <div>Nicholas Coltharp</div>
      <div>Email: coltharp@pdx.edu</div>
      <div>Office hours: TBA</div>
    </td></tr>
  </table>

<h3>Lecture</h3>
  <p>Lecture is Monday and Wednesday 6:40-8:30PM, in Engineering Building room 10 or at <a href="https://pdx.zoom.us/j/89397190069"></a>.</p>
  <div>Lecture recordings will be available on Canvas within 48 hours of each live lecture.</div>
  <p>If you attend in Zoom please <b>keep your microphone muted when you're not talking</b> so it doesn't pick up noise in your environment.</p>

<h3>Reading</h3>
  <div>There will be required readings posted to Canvas throughout the quarter.</div>

<h3>Course discussion forum</h3>
  <p>We'll be using the <a href="https://fishbowl.zulip.cs.pdx.edu/">Zulip server hosted by the CAT at PSU</a> for course discussion. You should have already received an invite to the <a href="https://fishbowl.zulip.cs.pdx.edu/#narrow/stream/295-crr-winter-2024">course discussion stream</a>. If you haven't received an invite, please email me!

<h3>Lab</h3>
  <p>In weeks 4, 7, and 10, you will work through collaborative lab exercises. These will take place in the Engineering Building classroom during our regular lecture time.</p>

  <p>Each individual student will <strong>only attend one lab session in each lab week</strong>. For example, you will attend lab <strong>either</strong> on Monday of week 4 <strong>or</strong> on Wednesday of week 4, <b>not both</b>. This means the classroom will be at most half full for each lab session. For the lab dates that you don't attend, there will be recorded lecture material on Canvas for you to watch.</p>
  <p>At the start of week 2, you will be randomly assigned to either a Monday or Wednesday lab session for each lab week. They might not all be the same day - for example, you might have lab on Monday of week 4 and Wednesday of week 7. You are <strong>required to attend in-person</strong> on your assigned lab dates, and you are required <strong>not</strong> to attend on the other lab dates.</p>
  <p>In the lab exercises, you will work in <strong>randomly-assigned</strong> small groups to complete collaborative exercises on the course topics. Most of your lab grade will come from simply coming prepared and participating in the exercises.</p>

  <p>To complete each lab, you will need to <strong>bring a laptop that you can install programs on</strong>. Almost any laptop from the past decade will be fine. Since Fall 2021, the upper-division CS program requires each student to have access to a laptop; if you don't have a laptop that you can bring to school, you may be able to <a href="https://library.pdx.edu/study-spaces-computers/equipment/">loan one from the PSU library</a>.</p>
  <p>The labs will use the <a href="https://www.typescriptlang.org/">TypeScript</a> language. You are <strong>not</strong> expected to know TypeScript already. We will cover TypeScript in lecture, and the first pre-lab assignment will walk through the process of setting up your TypeScript work environment.</p>
  <p>If you have accommodations from the Disability Resource Center that might affect your lab participation, just email me or come to my office hours and we'll figure out modified requirements that will work for you.</p>

<h3>Pre-lab assignments</h3>
  <p>There will be a pre-lab assignment for each lab, due on Sunday <strong>before</strong> the week of the lab. The pre-lab assignment must be submitted on time in order to participate in the lab.</p>

<h3>Post-lab writeups</h3>
  <p>After each lab you attend, you will be required to write a 1-2 page summary of the work that your team accomplished in the lab session. Each post-lab writeup is due one week after you attend the lab, on either Monday or Wednesday.</p>
  <p>You will <strong>not</strong> be graded based on what other students write about your participation in the lab groups. Your post-lab writeup will only be graded on whether you understood the core topics of the lab and made meaningful progress toward the goal of the exercise.</p>

<h3>Quizzes</h3>
  <p>Each week, there will be a "code review quiz" on Canvas designed to give you practice in the topics that we've discussed in lecture. These will usually involve both writing in English and editing some provided code in TypeScript.</p>
  <p>Most quiz responses will be graded on how well you justify your answers, not on whether you get the "right answers". There will often be certain answers that I expect, but it's fine if your answer is different than the one I expect as long as you can justify it.</p>

<h3>Course project</h3>
  <p>Graduate students are required to complete a course project. You may work alone or in groups of two or three, but no more than three.</p>

  <p>For the course project, you will select a <b>large codebase that at least one member of your group has previously worked on</b>. Your goal is to <b>significantly improve the quality and maintainability of the codebase</b>, according to the principles that we cover in class.</p>

  <p>At the end of the course, each graduate student will submit two project deliverables:
    <ul>
      <li>A link to a Git repository with your group's work, hosted on a site like GitLab or GitHub.</li>
      <li>A writeup explaining your own individual contributions to the project.</li>
    </ul>
  </p>

<h3>Grading</h3>
  <p>Grading will be very subjective for this course, since most of the material is subjective. For each piece of work you submit, you will receive feedback and a <b>letter grade</b>. Grades will be recorded on the Canvas page for this course. Final grades will be curved up at my discretion.</p>
  <p>In your final grade, each quiz, pre-lab, and post-lab grade is "worth" approximately the same amount. For graduate students, the final project grade is like two additional quiz grades.</p>
  <p>I expect to give most students final grades in the A to B range, so please don't stress too much about your grade - focus on the course content!</p>

</body>
</html>
